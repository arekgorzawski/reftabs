# Supercycle tables

----
# Organization
The directory tree follows:
* "prod" - the production supercycles
* "lab" - the lab/test supercycles

# Design Guide
* Place a supecycle table in "supercycle" directory of the "reftab" repository.
* Use "libreoffice" or "Microsoft Excel" to create/edit .csv files (comma-separated values).
* Each table should have a unique name {name}.csv.
* Fist "row" defines keys for each dataset within its column (key).
* All other "rows" are values expressed in their units.
* The keywords are unique: under Event and Trigger Reference [mevts-ess.yml](../init/mevts-ess.yml) and Data Buffer [databuffer-ess.yml](../init/databuffer-ess.yml).
* Each row requires a unique Id number (the reference purpose and crosscheck).

# Examples
* Check example-*.csv files for details.

# Appendix
* [More details](../doc)
* Beam Configuration: https://confluence.esss.lu.se/display/ABC/Beam+Configuration
* 20200130 Workshop Presentations: https://indico.esss.lu.se/event/1344/
