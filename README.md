# ESS Reference & Lookup Tables

----

## Table of Contents
- [Workflow](#Workflow)
- [Project Directory Structure](#Directory)

----
## Workflow <a name="Workflow"></a>


[![pipeline status](https://gitlab.esss.lu.se/hwcore/reftabs/badges/master/pipeline.svg)](https://gitlab.esss.lu.se/hwcore/reftabs/-/commits/master) `The "master" branch is mirrored into the timing master (TD-M).`

## Project Directory Structure <a name="Directory"></a>

### [init](init)

It loads during the initialization and contains ESS constant variables and enumerations for:

- [databuffer-ess](init/databuffer-ess.json) proton beam modes, destination and state according to ESS-0038258 (_Description of Modes for ESS Accelerator Operation_)

- [mevts-ess](init/mevts-ess.json) - master events according to: ESS-1837307 (_Time Structure of the Proton Beam Pulses in the LINAC_).

See [Beam Configuration](https://confluence.esss.lu.se/display/ABC/Beam+Configuration) for additional details.

### [supercycles](supercycles)

It loads upon the selection by an ESS operator (see the [manual](supecycles/README.md)). The dynamic routines/cycles (runtime).

### [tools](tools)

Productivity enhancements.

### [doc](doc)

Other documents, manuals, references and drafts.
